﻿using System;

namespace exercise15
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = (0.00);
            Console.WriteLine("what is your number?");
            Double.TryParse(Console.ReadLine(), out number);
            Console.WriteLine($"{number} * 1 = {number*1}");
            Console.WriteLine($"{number} * 2 = {number*2}");
            Console.WriteLine($"{number} * 3 = {number*3}");
            Console.WriteLine($"{number} * 4 = {number*4}");
            Console.WriteLine($"{number} * 5 = {number*5}");
            Console.WriteLine($"{number} * 6 = {number*6}");
            Console.WriteLine($"{number} * 7 = {number*7}");
            Console.WriteLine($"{number} * 8 = {number*8}");
            Console.WriteLine($"{number} * 9 = {number*9}");
            Console.WriteLine($"{number} * 10 = {number*10}");
            Console.WriteLine($"{number} * 11 = {number*11}");
            Console.WriteLine($"{number} * 12 = {number*12}");
        }
    }
}
